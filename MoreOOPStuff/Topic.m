//
//  Topic.m
//  MoreOOPStuff
//
//  Created by James Cash on 12-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "Topic.h"

@implementation Topic

#pragma mark - Initializer

- (instancetype)init
{
    self = [super init];
    if (self) {
        _oldTopics = [[NSMutableArray alloc] init];
        _currentTopic = @"<no topic set>";
    }
    return self;
}

#pragma mark - setter

- (void)setCurrentTopic:(NSString *)newTopic
{
    [self.oldTopics addObject:self.currentTopic];
    _currentTopic = newTopic;
}

#pragma mark - display

- (void)displayTopic
{
    NSLog(@"Topic: \"%@\"", self.currentTopic);
}


- (void)displayOldTopics
{
    NSLog(@"Previously:");
    for (NSString *topic in self.oldTopics) {
        NSLog(@" - \"%@\"", topic);
    }
}

@end
