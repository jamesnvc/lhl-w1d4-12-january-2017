//
//  main.m
//  MoreOOPStuff
//
//  Created by James Cash on 12-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Topic.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Topic *topic = [[Topic alloc] init];
        topic.currentTopic = @"Hello!";
        [topic displayTopic];
        topic.currentTopic = @"Bye!";
        [topic displayTopic];
        topic.currentTopic = @"Something else";
        [topic displayTopic];
        [topic displayOldTopics];
    }
    return 0;
}
