//
//  Topic.h
//  MoreOOPStuff
//
//  Created by James Cash on 12-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Topic : NSObject

@property (nonatomic,readonly,strong) NSMutableArray<NSString*>* oldTopics;
@property (nonatomic,strong) NSString* currentTopic;

- (void)displayTopic;
- (void)displayOldTopics;

@end
